using System.Security.Cryptography.X509Certificates;
using Constants;
using User;
namespace Services
{

    public static class MainServices
    {
        static Random random = new Random();
        public static bool CheckValue(int value, int max, int min = 0)
        {
            if (value > min && value < max) return true;
            else return false;
        }
        public static int CheckAnswer(int maxValue)
        {
            
            while (true)
            {
                bool ok = int.TryParse(Console.ReadLine(), out int answer);
                if (ok)
                {
                    if (!CheckValue(answer, maxValue))
                    {
                        TypeText("Incorrect choice\nTry again", 10);
                        continue;
                    }
                    return answer;
                }
                else
                {
                    TypeText("Incorrect choice\nTry again", 10);
                    continue;
                }

            }

            return 1;
        }

        public static void TypeText(string text, int timer)
        {
            foreach (var letter in text)
            {
                Console.Write(letter);
                Thread.Sleep(timer);
            }

            Console.Write("\n\n");
        }

        public static void SeeStatistics()
        {
            TypeText($"You played {UserProfile.gamesCounter} times\nYou won {UserProfile.wonCounter} times", 40);
        }
    }
    public static class GameServices
    {
        static Random random = new Random();
        public static void Game()
        {
            int winCounter = 0;
            int looseCounter = 0;
            for (int i = 0; i < 3; i++)
            {
                MainServices.TypeText(OptionConstants.chooseWinnerOption, 10);
                int choice = MainServices.CheckAnswer(4);
                int opponentChoice;
                do
                {
                    opponentChoice = random.Next(1, 4);
                } while (opponentChoice == choice);
                MainServices.TypeText($"You choose:\n{LogosConstants.logos[choice-1]}\n{MainConstants.participants[choice -1]}", 5);
                MainServices.TypeText($"Your opponent choose:\n{LogosConstants.logos[opponentChoice-1]}/n{MainConstants.participants[opponentChoice -1]}", 5);
                if ((choice == 1 && opponentChoice == 3) ||
                    (choice == 2 && opponentChoice == 1) ||
                    (choice == 3 && opponentChoice == 2))
                {
                    winCounter++;
                    Console.WriteLine("WON");
                }
                else
                {
                    looseCounter++;
                    Console.WriteLine("LOSE");
                }

                if (winCounter == 2 || looseCounter == 2)
                {
                    break;
                }
                
                
            }
            if (winCounter >= 2)
            {
                MainServices.TypeText($"VICTORY!!!\n{MainConstants.winMessages[random.Next(0,5)]}", 10);
                UserProfile.wonCounter++;
            }
            else
            {
                MainServices.TypeText($"DEFEAT!!!\n{MainConstants.loseMessages[random.Next(0,5)]}", 10);
            }
            UserProfile.gamesCounter ++;
            
        }
    }
    public static class UserServices
    {
        public static void EnterNickname()
        {
            MainServices.TypeText("Enter your Nickname", 20);
            string nick = Console.ReadLine();
            UserProfile.nickname = nick;
        }
        public static void EnterAge()
        {
            MainServices.TypeText($"Hi, {UserProfile.nickname}, now enter your age(12 -1000)", 20);
            while (true)
            {
                string strAge = Console.ReadLine();
                bool ok;
                ok = int.TryParse(strAge, out int age);
                if (ok)
                {
                    if (MainServices.CheckValue(age, 1000, UserConstants.minAge - 1))
                    {
                        UserProfile.age = age;
                        MainServices.TypeText($"Great, {UserProfile.nickname},you can continue", 20);
                        break;
                    }
                    else
                    {
                        MainServices.TypeText($"You are too young or too old, girls dont like that((\ud83d\ude45\u200d\u2640\ufe0f\n{UserProfile.nickname}, enter another age\ud83e\udd10", 20);
                        continue;
                    }
                }
                else
                {
                    MainServices.TypeText("Incorrect age\nTry again", 10);
                    continue;
                }
            }
        }
        public static void SeeOrChangeProfile()
        {
            while (true)
            {
                MainServices.TypeText($"Nickname: {UserProfile.nickname}\nAge: {UserProfile.age}\n", 20);
                MainServices.TypeText(OptionConstants.chooseChangeOption, 20);
                int choice = MainServices.CheckAnswer(4);
                switch (choice)
                {
                    case 1:
                    {
                        EnterNickname();
                        continue;
                    }
                    case 2:
                    {
                        EnterAge();
                        continue;
                    }
                    case 3:
                    {
                        MainServices.TypeText(OptionConstants.mainMenuOption, 20);
                        return;
                    }
                }
            }
        }

        
    }
    

}
