﻿using System;
using Constants;
using Services;
using User;


class MainClass
{
    static Random random = new Random(); 
    public static void Main(string[] agrs)
    {
        MainServices.TypeText(MainConstants.welcome, 5);
        MainServices.TypeText(MainConstants.greetingText, 8);
        MainServices.TypeText(MainConstants.girl, 1);
        UserServices.EnterNickname();
        UserServices.EnterAge();
        MainServices.TypeText(MainConstants.rules, 5);
        MainServices.TypeText(OptionConstants.mainMenuOption, 10);
        while (true)
        {
            int answer = MainServices.CheckAnswer(6);
            switch (answer)
            {
                case 1:
                {
                    GameServices.Game();
                    MainServices.TypeText(OptionConstants.gameOverOption, 10);
                    continue;
                }
                case 2:
                {
                    MainServices.TypeText(MainConstants.rules, 10);
                    MainServices.TypeText(OptionConstants.mainMenuOption, 10);
                    continue;
                }
                case 3:
                {
                    MainServices.SeeStatistics();
                    MainServices.TypeText(OptionConstants.mainMenuOption, 10);
                    continue;
                }
                case 4:
                { 
                    UserServices.SeeOrChangeProfile();
                    continue;
                }
                case 5:
                {
                    MainServices.TypeText(MainConstants.byeText, 10);
                    return;
                }
                
            }
        }
    }
}
